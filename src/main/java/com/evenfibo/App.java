package com.evenfibo;

/*
 * The problem is significantly signified due to the following observation. An integer which is produced as a sum of two integers
 * is even if both numbers are even or if both numbers are odd. Since F(1) = 1 (odd) and F(2) = 2 (even), F(3) is odd. Therefore F(4) = 5
 * is odd (F(2) even and F(3) odd) and F(5) is even. Each Fibonacci even term produces an odd term and both of them produce another odd term. 
 * Then the next term is even (since both previous term are odd). As a consequence, in order to calculate the sum of the even terms of the
 * Fibonacci sequence, we need to add every third term starting from F(2) = 2.
 */
public class App 
{
    public static void main( String[] args )
    {
        final int MAX_NUM = 4000000;
        
        // F(i-2)
        int firstNumber = 1;
        // F(i-1)
        int secondNumber = 2;
        // F(i)
        int newNumber = 3;
        // This counter is used to determine if newNumber is even
        int counter = 0;
        // The result - starts with 2 since F(2) = 2 is even
        int sum = 2;
        while(true) {
        	// Estimate new number and increment the counter
        	newNumber = firstNumber + secondNumber;
        	counter++;
        	// If the new number exceeds 4000000 return the result
        	if(newNumber > MAX_NUM) {
        		System.out.println("The sum of even Fibonacci numbers until " + MAX_NUM + " is: " + sum);
        		return;
        	}
        	// Check if this a new number - If that is the case, add the newNumber to the sum and reset the counter
        	if(counter == 3) {
        		sum += newNumber;
        		counter = 0;
        	}
        	// Shift the values of F(i-1) and F(i-2)
        	firstNumber = secondNumber;
        	secondNumber = newNumber;
        }
    }
}
